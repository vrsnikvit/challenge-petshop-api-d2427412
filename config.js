import { dirname, join } from 'path'
import { configure } from 'djorm/config.js'
import { fileURLToPath } from 'url'

import './db/models.js'

const baseDir = dirname(fileURLToPath(import.meta.url))

configure({
  apps: [
    join(baseDir, 'app.cjs'),
  ],
  databases: {
    default: {
      driver: 'djorm-db-sqlite',
      path: join(baseDir, 'db.sqlite'),
    },
  },
  logger: {
    level: 'debug',
  },
})
