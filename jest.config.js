export default {
  collectCoverageFrom: [
    '**/*.js',
    '!**/__fixtures__/**',
    '!**/static/**',
    '!**/coverage/**',
    '!jest.*',
  ],
  watchPlugins: [
    'jest-watch-select-projects',
    'jest-watch-typeahead/filename',
    'jest-watch-typeahead/testname',
  ],
  projects: [
    {
      displayName: 'integration',
      name: 'integration',
      roots: [ '<rootDir>' ],
      moduleFileExtensions: [ 'js', 'jsx', 'json', 'mjs', 'node' ],
      testPathIgnorePatterns: [
        '/__fixtures__/',
        '/coverage/',
        '/node_modules/',
        '/static/',
        '/dist/',
      ],
      transform: {
        '^.+\\.(js|jsx|mjs)$': 'babel-jest',
      },
    },
    {
      displayName: 'linter',
      name: 'linter',
      roots: [ '<rootDir>' ],
      moduleFileExtensions: [ 'js', 'jsx', 'json', 'mjs', 'node' ],
      testPathIgnorePatterns: [
        '/__fixtures__/',
        '/coverage/',
        '/node_modules/',
        '/static/',
        '/dist/',
      ],
      transform: {
        '^.+\\.(js|jsx|mjs)$': 'babel-jest',
      },
      runner: 'jest-runner-eslint',
      testMatch: [ '<rootDir>/**/*.{js,jsx}' ],
    },
  ],
}
