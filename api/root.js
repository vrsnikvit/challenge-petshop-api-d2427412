import { createRequire } from 'module'

const require = createRequire(import.meta.url)

export const root = (fastify) => {
  fastify.get('/', () => ({
    name: require('../package.json').name,
  }))
  return Promise.resolve()
}

