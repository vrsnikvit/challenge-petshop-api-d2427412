import { formatCollection } from './collections.js'
import { ObjectNotFound } from 'djorm/errors.js'
import { Ownership, Person } from '../db/models.js'

export const personObject = (fastify) => {
  fastify.get('/', async req =>
    await Person.objects.get({ id: req.params.personId })
  )

  fastify.get('/pets', async req => {
    const ownerships = await Ownership.objects
      .filter({ ownerId: req.params.personId })
      .selectRelated('pet')
      .all()
    return formatCollection(ownerships.map(ownership => ownership.pet))
  })

  fastify.route({
    method: 'PATCH',
    url: '/',
    schema: {
      body: { $ref: 'models/Person' },
    },
    handler: async req => {
      const person = await Person.objects.get({ id: req.params.personId })
      person.setValues(req.body)
      return await person.save()
    },
  })

  fastify.delete('/', async (req, res) => {
    try {
      const ownerships = await Ownership.objects.filter({ ownerid: req.params.personId }) //deleting ownership if the person is deleted
          .all()
      await ownerships.forEach(ownership => {
        ownership.delete()
      });
      const person = await Person.objects.get({ id: req.params.personId })
      await person.delete()
    } catch (e) {
      if (!(e instanceof ObjectNotFound)) {
        throw e
      }
    }
    res.status(204)
  })
  return Promise.resolve()
}

export const personCollection = (fastify) => {
  fastify.register(personObject, { prefix: '/:personId' })
  fastify.addSchema({
    $id: 'models/Person',
    type: 'object',
    required: ['name'],
    properties: {
      name: {
        type: 'string',
      },
    },
  })

  fastify.get('/', async () => formatCollection(await Person.objects.all()))

  fastify.route({
    method: 'POST',
    url: '/',
    schema: {
      body: { $ref: 'models/Person' },
    },
    handler: async (req, res) => {
      const person = await Person.create(req.body)
      res.status(201)
      return person
    },
  })
  return Promise.resolve()
}

