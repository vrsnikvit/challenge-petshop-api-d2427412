import { formatCollection } from './collections.js'
import { Ownership } from '../db/models.js'

export const ownershipObject = (fastify) => {
  fastify.get('/', async req =>
    await Ownership.objects.get({ id: req.params.ownershipId })
  )
   //add DELETE endpoint
   fastify.delete('/', async (req, res) => {
    try {
      const ownership = await Ownership.objects.get({ id: req.params.ownershipId })
      await ownership.delete()
    } catch (e) {
      if (!(e instanceof ObjectNotFound)) {
        throw e
      }
    }
    res.status(204)
  })

   return Promise.resolve()
}

export const ownershipCollection = (fastify) => {
  fastify.register(ownershipObject, { prefix: '/:ownershipId' })
  fastify.addSchema({
    $id: 'models/Ownership',
    type: 'object',
    required: ['ownerId', 'petId'],
    properties: {
      ownerId: {
        type: 'array',
      },
      petId: {
        type: 'array',
      },
    },
  })

  fastify.get('/', async () => formatCollection(await Ownership.objects.all()))
  
  // add POST endpoint
  fastify.route({
    method: 'POST',
    url: '/',
    schema: {
      body: { $ref: 'models/Ownership' },
    },
    handler: async (req, res) => {
      const ownership = await Ownership.create(req.body)
      res.status(201)
      return ownership
    },
  })

  return Promise.resolve()
}

