import { formatCollection } from './collections.js'
import { ObjectNotFound } from 'djorm/errors.js'
import { Pet } from '../db/models.js'

export const petObject = (fastify) => {
  fastify.get('/', async req =>
    await Pet.objects.get({ id: req.params.petId })
  )

  fastify.route({
    method: 'PATCH',
    url: '/',
    schema: {
      body: { $ref: 'models/Pet' },
    },
    handler: async req => {
      const pet = await Pet.objects.get({ id: req.params.petId })
      pet.setValues(req.body)
      return await pet.save()
    },
  })

  fastify.delete('/', async (req, res) => {
    try {
      const ownerships = await Ownership.objects.filter({ petId: req.params.petId }) //deleting ownership if the pet is deleted
          .all()
      await ownerships.forEach(ownership => {
        ownership.delete()
      });
      const pet = await Pet.objects.get({ id: req.params.petId })
      await pet.delete()
    } catch (e) {
      if (!(e instanceof ObjectNotFound)) {
        throw e
      }
    }
    res.status(204)
  })
  return Promise.resolve()
}

export const petCollection = (fastify) => {
  fastify.register(petObject, { prefix: '/:petId' })
  fastify.addSchema({
    $id: 'models/Pet',
    type: 'object',
    required: ['animal', 'name', 'birthday'],
    properties: {
      animal: {
        type: 'string',
      },
      name: {
        type: 'string',
      },
      birthday: {
        type: 'string',
        format: 'date',
      },
    },
  })

  fastify.get('/', async () => formatCollection(await Pet.objects.all()))
  fastify.route({
    method: 'POST',
    url: '/',
    schema: {
      body: { $ref: 'models/Pet' },
    },
    handler: async (req, res) => {
      const pet = await Pet.create(req.body)
      res.status(201)
      return pet
    },
  })
  return Promise.resolve()
}

