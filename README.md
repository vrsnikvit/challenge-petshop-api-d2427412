# Petshop API

This API project is a testing ground for people coming to know fastify. It most
probably carries no value to your project.

Please see the [OpenAPI specs](./openapi.yaml) to see the API details. Read the
[Contributing guide](./CONTRIBUTING.md) before contributing to this project.

## Installation

Make sure, your system has following resources before installation:

* [node-16](https://nodejs.org/en/download/)

Then run:

```shell
npm ci
```

## Running

To start the API locally, simply run:

```shell
npm start
```

To preview the generated Open API specifications, run:

```shell
npm run specs
```

## Testing

To run linter against your code, run:

```shell
npm run lint
```
