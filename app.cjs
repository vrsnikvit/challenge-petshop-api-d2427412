const { access, copyFile } = require('fs/promises')
const { join } = require('path')

const logger = require('djorm/logger')

const init = async () => {
  const dest = join(__dirname, 'db.sqlite')
  try {
    await access(dest)
    logger.info(`Reusing database in ${dest}`)
  } catch (e) {
    const src = join(__dirname, 'db', 'empty.sqlite')
    await copyFile(src, dest)
    logger.info(`Initialized database in ${dest}`)
  }
}

const shutdown = () => {}

module.exports = { init, shutdown }
