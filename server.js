import createFastify from 'fastify'
import logger from 'djorm/logger.js'

import { init } from 'djorm/config.js'
import { ownershipCollection } from './api/ownerships.js'
import { personCollection } from './api/people.js'
import { petCollection } from './api/pets.js'
import { root } from './api/root.js'

import './config.js'

const port = process.env.NODE_PORT || 3000
const fastify = createFastify({ logger: true })

fastify.addContentTypeParser('application/json', { parseAs: 'string' }, (req, body, done) => {
  try {
    done(null, JSON.parse(body))
  } catch (err) {
    err.statusCode = 400
    done(err)
  }
})

fastify.register(ownershipCollection, { prefix: '/ownerships' })
fastify.register(petCollection, { prefix: '/pets' })
fastify.register(personCollection, { prefix: '/people' })
fastify.register(root)

const start = async () => {
  try {
    await init()
    await fastify.listen(port)
  } catch (err) {
    logger.error(err)
    process.exit(255)
  }
}


await start()
