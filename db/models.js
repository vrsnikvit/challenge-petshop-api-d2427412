import { DatabaseModel } from 'djorm/models/index.js'
import {
  DateField,
  DateTimeField,
  CharField,
  ForeignKey,
  PositiveIntegerField,
} from 'djorm/fields/index.js'

class TimestampedModel extends DatabaseModel {
  static id = new PositiveIntegerField()
  static createdAt = new DateTimeField()
  static updatedAt = new DateTimeField({ null: true })
  static meta = {
    abstract: true,
  }

  async create () {
    this.createdAt = new Date()
    return await super.create()
  }

  async update () {
    this.updatedAt = new Date()
    return await super.update()
  }
}

export class Pet extends TimestampedModel {
  static animal = new CharField()
  static name = new CharField()
  static birthday = new DateField()
  static meta = {
    modelName: 'Pet',
  }
}

export class Person extends TimestampedModel {
  static name = new CharField()
  static meta = {
    modelName: 'Person',
  }
}

export class Ownership extends TimestampedModel {
  static owner = new ForeignKey({
    model: 'Person',
    keyField: 'ownerId',
    relatedName: 'ownerships',
  })
  static pet = new ForeignKey({
    model: 'Pet',
    keyField: 'petId',
    relatedName: 'ownerships',
  })
}

Pet.register()
Person.register()
Ownership.register()
