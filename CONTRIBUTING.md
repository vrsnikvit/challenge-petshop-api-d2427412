## General requirements

* Code must work on version of node mentioned in [the requirements](./README.md)
* The must API match the [OpenAPI specifications](./openapi.yaml)
* Application must persist the state in a local SQLite database
* Consult pulling in any external dependencies
* Use [@optimics/eslint-config](https://npm.js.com/package/@optimics/eslint-config) coding style
* Comment your code meaningfully
* Fork this repo and create a pull request with your changes
